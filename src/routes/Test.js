const handleSubmit = () => {
    const kp = Pact.crypto.genKeyPair();
    var creationTime = () => Math.round(new Date().getTime() / 1000) - 15;

    const cmd = {
        networkId: "testnet04",
        pactCode: `(free.basic-payment.use-let)`,
        keyPairs: [
        {
            publicKey: kp.publicKey,
            secretKey: kp.secretKey,
            clist: [
            //capability to use gas station
            {
                name: `free.basic-payment-gas-station.GAS_PAYER`,
                //args are irrelevant here just need to be the right type
                args: ["hi", { int: 1 }, 1.0],
            },
            ],
        },
        ],
        //pact-lang-api function to construct transaction meta data
        meta: Pact.lang.mkMeta(
            "Admin", //sender
            "1", //chainid
            0.00010001, //gas price
            30000, // gas limit
            creationTime(), // creation
            28800 // ttl
        ),
    }

    console.log(cmd)

    const tx = await Pact.fetch.send(cmd, kadenaAPI.meta.host);
    console.log(tx)
}


 const cmd = {
   pactCode: `(coin.transfer "k:011cd755bcd7428fb560c738da605146dedf9237f9c18af93a2756a933ef51a7" "Alice" 1.0)`,
   caps: [
     {
       role: "Some Role",
       description: "Some desc..",
       args: {
         name: "coin.GAS",
         arg: [],
       },
     },
     {
       role: "Some Role",
       description: "Some desc..",
       args: {
         name: "coin.TRANSFER",
         arg: [
           "k:011cd755bcd7428fb560c738da605146dedf9237f9c18af93a2756a933ef51a7",
           "Alice",
           1.0,
         ],
       },
     },
   ],
   envData: keysets,
   sender: sender,
   chainId: kadenaAPI.meta.chainId,
   gasLimit: kadenaAPI.meta.gasLimit,
   gasPrice: kadenaAPI.meta.gasPrice,
   signingPubKey:
     "011cd755bcd7428fb560c738da605146dedf9237f9c18af93a2756a933ef51a7", // account with no prefix k here
   networkId: kadenaAPI.meta.networkId,
   nonce: kadenaAPI.meta.nonce,
 };

 console.log(cmd);

 try {
   setResult("");
   setError(false);

   const signedReq = await Pact.wallet.sign(cmd);
   console.log(signedReq);

   const tx = await Pact.wallet.sendSigned(signedReq, kadenaAPI.meta.host);
   console.log(tx);

   setRequestKey(tx.requestKeys[0]);
 } catch (error) {
   setError(error);
   setIsLoading(false);
   setResult("");
 }

