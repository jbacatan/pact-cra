import React from 'react'
import CoinbaseCommerceButton from "react-coinbase-commerce";
import "react-coinbase-commerce/dist/coinbase-commerce-button.css";

const Payment = () => {
    const handlePayButton = async () => {    
        const coinAPIKey = "25e1fb3c-3e2b-428e-aea5-c16798f3b6c3";
        const host = "https://api.commerce.coinbase.com/charges";

        const request = {
            "name":"The Human Fund",
            "description":"Money For People",
            "pricing_type":"no_price"
        }

        const result = await fetch(host, {
            headers: {
                "Content-Type": "application/json",
                "X-CC-Api-Key": coinAPIKey,
                "X-CC-Version": "2018-03-22" 
            },
            data: JSON.stringify(request)
        })

        const data = await result.json()
        console.log(data)
    }

  return (
    <main className="md:w-4/5 mx-auto p-4 md:p-6">
      <section className="w-full mb-5">
        <h1 className="text-lg font-medium">CoinBase Crypto Payments</h1>
      </section>

      <div className="flex justify-center items-center w-full">
        <div className="md:w-1/4 h-96 border rounded p-8">
          <h2 className="font-bold mt-2 mb-8">Medicine Title</h2>
          <ul className="ml-5 text-sm text-gray-600">
            <li>Description</li>
            <li>Description</li>
            <li>Description</li>
            <li>Description</li>
          </ul>
          <p className="mt-10 text-center text-xl font-bold">$10.00</p>
          <button
            className="block mx-auto py-2 px-5 mt-10 bg-indigo-500 rounded text-white shadow"
            onClick={handlePayButton}
          >
            Pay Crypto
          </button>

          <CoinbaseCommerceButton checkoutId={'JXBFM8AA'} />
        </div>
      </div>
    </main>
  );
}

export default Payment