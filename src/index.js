import React from "react";
import ReactDOM from "react-dom";
import { HashRouter, Routes, Route } from "react-router-dom";

import "./index.css"

import App from "./App";
import Methods from "./routes/Methods";
import Keypair from "./routes/Keypair";
import Contract from "./routes/Contract";
import Payment from "./routes/Payment";

const rootElement = document.getElementById("root");
ReactDOM.render(
  <HashRouter>
    <Routes>
      <Route path="/" element={<App />}>
        <Route path="/methods" element={<Methods />} />
        <Route path="/keypair" element={<Keypair />} />
        <Route path="/contract" element={<Contract />} />
        <Route path="/payment" element={<Payment />} />
      </Route>
    </Routes>
  </HashRouter>,
  rootElement
);