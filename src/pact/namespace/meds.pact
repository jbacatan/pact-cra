(define-keyset 'meds-ns-users)
(define-keyset 'meds-ns-admin)

(define-namespace 'meds
  (keyset-ref-guard 'meds-ns-users)
  (keyset-ref-guard 'meds-ns-admin))
