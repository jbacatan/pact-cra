(namespace 'meds)

(module basic-guards GOVERNANCE
    ;; <--- Table --->
    (defschema user
      name: string
      age: integer
      role: string
      guard: guard)
    
    (deftable tbl_user: {user})


    ;; <--- Capability --->
    (defcap GOVERNANCE()
      (enforce-guard (read-keyset "meds-admin-ks"))
    )
    
    ;; <--- Logic --->
    (defun welcome-message()
        (format "Welcome to pact, hope your doing fine!" [])
    )

    (defun create-account:string(id:string name:string age:integer role:string guard:guard)
        (enforce-guard (read-keyset "meds-admin-ks"))
        (enforce (!= "" name) "Name is required")
        (enforce (!= "" age) "Age is required")
        (enforce (!= "" role) "Role is required")

        (insert tbl_user id {
            'name: name,
            'age: age,
            'role: role,
            'guard: guard
        })

        (format "Account created with id: {}" [id])
    )

    (defun account-details (id) 
        (enforce-guard (at 'guard (read tbl_user id)))
        (read tbl_user id)
    )

)

(create-table tbl_user)


